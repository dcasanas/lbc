lbc: A fully parameterizable leading-bit counter (either zero or one)
implemented in SystemVerilog.
