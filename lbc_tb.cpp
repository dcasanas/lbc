#include <iostream>
#include <stdlib.h>
#include <verilated.h>
#include <verilated_vcd_c.h>
#include "cpp_src/lbc_0.h"
#include "cpp_src/lbc_1.h"

#define SIM_STEP 		20e3
#define SIM_TIME_MAX 	SIM_STEP * 10 

vluint64_t sim_time = 0;

int main (int argc, char ** argv, char ** env)
{
	// Instantiate module classes.
	lbc_0 * dut_0 = new lbc_0;
	lbc_1 * dut_1 = new lbc_1;
	
	Verilated::traceEverOn(true);
	
	VerilatedVcdC * m_trace = new VerilatedVcdC;

	dut_0 	-> trace (m_trace, 5);
	dut_1 	-> trace (m_trace, 5);
	
	m_trace -> open ("waveform.vcd");
	
	while (sim_time < SIM_TIME_MAX)
	{
	
		if (sim_time == SIM_STEP)
		{
			dut_0 -> vec_i = 0b00000000;
			dut_1 -> vec_i = 0b00000000;	
		}	
			
		if (sim_time == SIM_STEP * 2)
		{
			dut_0 -> vec_i = 0b00000001;
			dut_1 -> vec_i = 0b10000000;
		}
			
		if (sim_time == SIM_STEP * 3)
		{
			dut_0 -> vec_i = 0b00000010;
			dut_1 -> vec_i = 0b11000000;
		}
		
		if (sim_time == SIM_STEP * 4)
		{
			dut_0 -> vec_i = 0b00000100;
			dut_1 -> vec_i = 0b11100000;
		}
		
		if (sim_time == SIM_STEP * 5)
		{
			dut_0 -> vec_i = 0b00001000;
			dut_1 -> vec_i = 0b11110000;
		}
		
		if (sim_time == SIM_STEP * 6)
		{
			dut_0 -> vec_i = 0b00010000;
			dut_1 -> vec_i = 0b11111000;
		}
		
		if (sim_time == SIM_STEP * 7)
		{
			dut_0 -> vec_i = 0b00100000;
			dut_1 -> vec_i = 0b11111100;
		}
		
		if (sim_time == SIM_STEP * 8)
		{
			dut_0 -> vec_i = 0b01000000;
			dut_1 -> vec_i = 0b11111110;
		}
		
		if (sim_time == SIM_STEP * 9)
		{
			dut_0 -> vec_i = 0b10000000;
			dut_1 -> vec_i = 0b11111111;
		}
			
		dut_0 	-> eval ();
		dut_1 	-> eval ();
		m_trace -> dump (sim_time);
		sim_time++;
	}

	m_trace -> close ();
	delete dut_1;
	delete dut_0;
	exit (EXIT_SUCCESS);
}
