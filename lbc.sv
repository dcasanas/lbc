// Module: ldb.sv (Leading-bits counter)
// Author: Daniel Casañas.
module lbc
# (
	parameter IN_WIDTH 	= 64,
	parameter OUT_WIDTH = $clog2 (IN_WIDTH),
	
	// Bit-in-question (0 or 1).
	parameter BIQ  		= 1'b0	
)
(
	input wire [IN_WIDTH - 1:0] 	vec_i,
	
	output wire [OUT_WIDTH - 1:0] 	cnt_o
);

	generate
		// Base case.
		if (IN_WIDTH == 2)
			assign cnt_o = (BIQ ? vec_i[1] : !vec_i[1]);
			
		// Recursive case.
		else
			begin
				wire [(IN_WIDTH / 2) - 1:0] l_half 	= vec_i[(IN_WIDTH / 2) +: (IN_WIDTH / 2)];
				wire [(IN_WIDTH / 2) - 1:0] r_half 	= vec_i[0 +: (IN_WIDTH / 2)];
				wire [OUT_WIDTH  - 2:0] 	cnt_half;
				
				// Use reduction AND and OR to check whether the
				// entire left half is ones or zeroes. If the 
				// condition holds, we can add IN_WIDTH / 2 to
				// cnt_o safely.
				wire is_l_half_full = (BIQ ? & l_half : ~| l_half);
				
				lbc # (.IN_WIDTH (IN_WIDTH / 2), .BIQ(BIQ)) lbc_inst
				(
					.vec_i (is_l_half_full ? r_half : l_half),
					
					.cnt_o (cnt_half)
				);
				
				// is_l_half_full accounts for the entire left
				// half, therefore its state can be used to form
				// the final cnt_o value.
				assign cnt_o = {is_l_half_full, cnt_half};
			end
	
	endgenerate

endmodule
