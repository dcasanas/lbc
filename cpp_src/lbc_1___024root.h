// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See lbc_1.h for the primary calling header

#ifndef VERILATED_LBC_1___024ROOT_H_
#define VERILATED_LBC_1___024ROOT_H_  // guard

#include "verilated.h"

class lbc_1__Syms;

class lbc_1___024root final : public VerilatedModule {
  public:

    // DESIGN SPECIFIC STATE
    VL_IN8(vec_i,7,0);
    VL_OUT8(cnt_o,2,0);
    CData/*3:0*/ lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i;
    VlUnpacked<CData/*0:0*/, 2> __Vm_traceActivity;

    // INTERNAL VARIABLES
    lbc_1__Syms* const vlSymsp;

    // CONSTRUCTORS
    lbc_1___024root(lbc_1__Syms* symsp, const char* name);
    ~lbc_1___024root();
    VL_UNCOPYABLE(lbc_1___024root);

    // INTERNAL METHODS
    void __Vconfigure(bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
