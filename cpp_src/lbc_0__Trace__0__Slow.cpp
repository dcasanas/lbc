// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "lbc_0__Syms.h"


VL_ATTR_COLD void lbc_0___024root__trace_init_sub__TOP__0(lbc_0___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_0__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_0___024root__trace_init_sub__TOP__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+8,"vec_i", false,-1, 7,0);
    tracep->declBus(c+9,"cnt_o", false,-1, 2,0);
    tracep->pushNamePrefix("lbc ");
    tracep->declBus(c+13,"OUT_WIDTH", false,-1, 31,0);
    tracep->declBus(c+8,"vec_i", false,-1, 7,0);
    tracep->declBus(c+9,"cnt_o", false,-1, 2,0);
    tracep->pushNamePrefix("genblk1 ");
    tracep->declBus(c+10,"l_half", false,-1, 3,0);
    tracep->declBus(c+11,"r_half", false,-1, 3,0);
    tracep->declBus(c+1,"cnt_half", false,-1, 1,0);
    tracep->declBit(c+12,"is_l_half_full", false,-1);
    tracep->pushNamePrefix("lbc_inst ");
    tracep->declBus(c+14,"IN_WIDTH", false,-1, 31,0);
    tracep->declBus(c+15,"OUT_WIDTH", false,-1, 31,0);
    tracep->declBus(c+16,"BIQ", false,-1, 0,0);
    tracep->declBus(c+2,"vec_i", false,-1, 3,0);
    tracep->declBus(c+1,"cnt_o", false,-1, 1,0);
    tracep->pushNamePrefix("genblk1 ");
    tracep->declBus(c+3,"l_half", false,-1, 1,0);
    tracep->declBus(c+4,"r_half", false,-1, 1,0);
    tracep->declBus(c+5,"cnt_half", false,-1, 0,0);
    tracep->declBit(c+6,"is_l_half_full", false,-1);
    tracep->pushNamePrefix("lbc_inst ");
    tracep->declBus(c+15,"IN_WIDTH", false,-1, 31,0);
    tracep->declBus(c+17,"OUT_WIDTH", false,-1, 31,0);
    tracep->declBus(c+16,"BIQ", false,-1, 0,0);
    tracep->declBus(c+7,"vec_i", false,-1, 1,0);
    tracep->declBus(c+5,"cnt_o", false,-1, 0,0);
    tracep->popNamePrefix(5);
}

VL_ATTR_COLD void lbc_0___024root__trace_init_top(lbc_0___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_0__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_0___024root__trace_init_top\n"); );
    // Body
    lbc_0___024root__trace_init_sub__TOP__0(vlSelf, tracep);
}

VL_ATTR_COLD void lbc_0___024root__trace_full_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp);
void lbc_0___024root__trace_chg_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp);
void lbc_0___024root__trace_cleanup(void* voidSelf, VerilatedVcd* /*unused*/);

VL_ATTR_COLD void lbc_0___024root__trace_register(lbc_0___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_0__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_0___024root__trace_register\n"); );
    // Body
    tracep->addFullCb(&lbc_0___024root__trace_full_top_0, vlSelf);
    tracep->addChgCb(&lbc_0___024root__trace_chg_top_0, vlSelf);
    tracep->addCleanupCb(&lbc_0___024root__trace_cleanup, vlSelf);
}

VL_ATTR_COLD void lbc_0___024root__trace_full_sub_0(lbc_0___024root* vlSelf, VerilatedVcd::Buffer* bufp);

VL_ATTR_COLD void lbc_0___024root__trace_full_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_0___024root__trace_full_top_0\n"); );
    // Init
    lbc_0___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<lbc_0___024root*>(voidSelf);
    lbc_0__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    lbc_0___024root__trace_full_sub_0((&vlSymsp->TOP), bufp);
}

VL_ATTR_COLD void lbc_0___024root__trace_full_sub_0(lbc_0___024root* vlSelf, VerilatedVcd::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_0__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_0___024root__trace_full_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode);
    // Body
    bufp->fullCData(oldp+1,(((2U & ((~ (IData)((0U 
                                                != 
                                                (3U 
                                                 & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                    >> 2U))))) 
                                    << 1U)) | (1U & 
                                               (~ (1U 
                                                   & (((0U 
                                                        != 
                                                        (3U 
                                                         & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                            >> 2U)))
                                                        ? 
                                                       ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                        >> 2U)
                                                        : (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i)) 
                                                      >> 1U)))))),2);
    bufp->fullCData(oldp+2,(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i),4);
    bufp->fullCData(oldp+3,((3U & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                   >> 2U))),2);
    bufp->fullCData(oldp+4,((3U & (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i))),2);
    bufp->fullBit(oldp+5,((1U & (~ (1U & (((0U != (3U 
                                                   & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                      >> 2U)))
                                            ? ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                               >> 2U)
                                            : (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i)) 
                                          >> 1U))))));
    bufp->fullBit(oldp+6,((1U & (~ (IData)((0U != (3U 
                                                   & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                      >> 2U))))))));
    bufp->fullCData(oldp+7,((3U & ((0U != (3U & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                 >> 2U)))
                                    ? ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                       >> 2U) : (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i)))),2);
    bufp->fullCData(oldp+8,(vlSelf->vec_i),8);
    bufp->fullCData(oldp+9,(vlSelf->cnt_o),3);
    bufp->fullCData(oldp+10,((0xfU & ((IData)(vlSelf->vec_i) 
                                      >> 4U))),4);
    bufp->fullCData(oldp+11,((0xfU & (IData)(vlSelf->vec_i))),4);
    bufp->fullBit(oldp+12,((1U & (~ (IData)((0U != 
                                             (0xfU 
                                              & ((IData)(vlSelf->vec_i) 
                                                 >> 4U))))))));
    bufp->fullIData(oldp+13,(3U),32);
    bufp->fullIData(oldp+14,(4U),32);
    bufp->fullIData(oldp+15,(2U),32);
    bufp->fullBit(oldp+16,(0U));
    bufp->fullIData(oldp+17,(1U),32);
}
