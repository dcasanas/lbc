// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See lbc_0.h for the primary calling header

#include "verilated.h"

#include "lbc_0__Syms.h"
#include "lbc_0___024root.h"

void lbc_0___024root___ctor_var_reset(lbc_0___024root* vlSelf);

lbc_0___024root::lbc_0___024root(lbc_0__Syms* symsp, const char* name)
    : VerilatedModule{name}
    , vlSymsp{symsp}
 {
    // Reset structure values
    lbc_0___024root___ctor_var_reset(this);
}

void lbc_0___024root::__Vconfigure(bool first) {
    if (false && first) {}  // Prevent unused
}

lbc_0___024root::~lbc_0___024root() {
}
