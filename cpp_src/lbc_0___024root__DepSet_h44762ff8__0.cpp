// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See lbc_0.h for the primary calling header

#include "verilated.h"

#include "lbc_0___024root.h"

VL_INLINE_OPT void lbc_0___024root___combo__TOP__0(lbc_0___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_0__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_0___024root___combo__TOP__0\n"); );
    // Body
    vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i 
        = (0xfU & ((0U != (0xfU & ((IData)(vlSelf->vec_i) 
                                   >> 4U))) ? ((IData)(vlSelf->vec_i) 
                                               >> 4U)
                    : (IData)(vlSelf->vec_i)));
    vlSelf->cnt_o = ((4U & ((~ (IData)((0U != (0xfU 
                                               & ((IData)(vlSelf->vec_i) 
                                                  >> 4U))))) 
                            << 2U)) | ((2U & ((~ (IData)(
                                                         (0U 
                                                          != 
                                                          (3U 
                                                           & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                              >> 2U))))) 
                                              << 1U)) 
                                       | (1U & (~ (1U 
                                                   & (((0U 
                                                        != 
                                                        (3U 
                                                         & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                            >> 2U)))
                                                        ? 
                                                       ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                        >> 2U)
                                                        : (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i)) 
                                                      >> 1U))))));
}

void lbc_0___024root___eval(lbc_0___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_0__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_0___024root___eval\n"); );
    // Body
    lbc_0___024root___combo__TOP__0(vlSelf);
    vlSelf->__Vm_traceActivity[1U] = 1U;
}

#ifdef VL_DEBUG
void lbc_0___024root___eval_debug_assertions(lbc_0___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_0__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_0___024root___eval_debug_assertions\n"); );
}
#endif  // VL_DEBUG
