// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Model implementation (design independent parts)

#include "lbc_1.h"
#include "lbc_1__Syms.h"
#include "verilated_vcd_c.h"

//============================================================
// Constructors

lbc_1::lbc_1(VerilatedContext* _vcontextp__, const char* _vcname__)
    : VerilatedModel{*_vcontextp__}
    , vlSymsp{new lbc_1__Syms(contextp(), _vcname__, this)}
    , vec_i{vlSymsp->TOP.vec_i}
    , cnt_o{vlSymsp->TOP.cnt_o}
    , rootp{&(vlSymsp->TOP)}
{
    // Register model with the context
    contextp()->addModel(this);
}

lbc_1::lbc_1(const char* _vcname__)
    : lbc_1(Verilated::threadContextp(), _vcname__)
{
}

//============================================================
// Destructor

lbc_1::~lbc_1() {
    delete vlSymsp;
}

//============================================================
// Evaluation loop

void lbc_1___024root___eval_initial(lbc_1___024root* vlSelf);
void lbc_1___024root___eval_settle(lbc_1___024root* vlSelf);
void lbc_1___024root___eval(lbc_1___024root* vlSelf);
#ifdef VL_DEBUG
void lbc_1___024root___eval_debug_assertions(lbc_1___024root* vlSelf);
#endif  // VL_DEBUG
void lbc_1___024root___final(lbc_1___024root* vlSelf);

static void _eval_initial_loop(lbc_1__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    lbc_1___024root___eval_initial(&(vlSymsp->TOP));
    // Evaluate till stable
    vlSymsp->__Vm_activity = true;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Initial loop\n"););
        lbc_1___024root___eval_settle(&(vlSymsp->TOP));
        lbc_1___024root___eval(&(vlSymsp->TOP));
    } while (0);
}

void lbc_1::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate lbc_1::eval_step\n"); );
#ifdef VL_DEBUG
    // Debug assertions
    lbc_1___024root___eval_debug_assertions(&(vlSymsp->TOP));
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    vlSymsp->__Vm_activity = true;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        lbc_1___024root___eval(&(vlSymsp->TOP));
    } while (0);
    // Evaluate cleanup
}

//============================================================
// Utilities

const char* lbc_1::name() const {
    return vlSymsp->name();
}

//============================================================
// Invoke final blocks

VL_ATTR_COLD void lbc_1::final() {
    lbc_1___024root___final(&(vlSymsp->TOP));
}

//============================================================
// Implementations of abstract methods from VerilatedModel

const char* lbc_1::hierName() const { return vlSymsp->name(); }
const char* lbc_1::modelName() const { return "lbc_1"; }
unsigned lbc_1::threads() const { return 1; }
std::unique_ptr<VerilatedTraceConfig> lbc_1::traceConfig() const {
    return std::unique_ptr<VerilatedTraceConfig>{new VerilatedTraceConfig{false, false, false}};
};

//============================================================
// Trace configuration

void lbc_1___024root__trace_init_top(lbc_1___024root* vlSelf, VerilatedVcd* tracep);

VL_ATTR_COLD static void trace_init(void* voidSelf, VerilatedVcd* tracep, uint32_t code) {
    // Callback from tracep->open()
    lbc_1___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<lbc_1___024root*>(voidSelf);
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (!vlSymsp->_vm_contextp__->calcUnusedSigs()) {
        VL_FATAL_MT(__FILE__, __LINE__, __FILE__,
            "Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    }
    vlSymsp->__Vm_baseCode = code;
    tracep->scopeEscape(' ');
    tracep->pushNamePrefix(std::string{vlSymsp->name()} + ' ');
    lbc_1___024root__trace_init_top(vlSelf, tracep);
    tracep->popNamePrefix();
    tracep->scopeEscape('.');
}

VL_ATTR_COLD void lbc_1___024root__trace_register(lbc_1___024root* vlSelf, VerilatedVcd* tracep);

VL_ATTR_COLD void lbc_1::trace(VerilatedVcdC* tfp, int levels, int options) {
    if (false && levels && options) {}  // Prevent unused
    tfp->spTrace()->addModel(this);
    tfp->spTrace()->addInitCb(&trace_init, &(vlSymsp->TOP));
    lbc_1___024root__trace_register(&(vlSymsp->TOP), tfp->spTrace());
}
