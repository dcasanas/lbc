// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See lbc_1.h for the primary calling header

#include "verilated.h"

#include "lbc_1__Syms.h"
#include "lbc_1___024root.h"

void lbc_1___024root___ctor_var_reset(lbc_1___024root* vlSelf);

lbc_1___024root::lbc_1___024root(lbc_1__Syms* symsp, const char* name)
    : VerilatedModule{name}
    , vlSymsp{symsp}
 {
    // Reset structure values
    lbc_1___024root___ctor_var_reset(this);
}

void lbc_1___024root::__Vconfigure(bool first) {
    if (false && first) {}  // Prevent unused
}

lbc_1___024root::~lbc_1___024root() {
}
