// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See lbc_1.h for the primary calling header

#include "verilated.h"

#include "lbc_1___024root.h"

VL_INLINE_OPT void lbc_1___024root___combo__TOP__0(lbc_1___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root___combo__TOP__0\n"); );
    // Body
    vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i 
        = (0xfU & ((0xfU == (0xfU & ((IData)(vlSelf->vec_i) 
                                     >> 4U))) ? (IData)(vlSelf->vec_i)
                    : ((IData)(vlSelf->vec_i) >> 4U)));
    vlSelf->cnt_o = (((IData)((0xfU == (0xfU & ((IData)(vlSelf->vec_i) 
                                                >> 4U)))) 
                      << 2U) | (((IData)((3U == (3U 
                                                 & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                    >> 2U)))) 
                                 << 1U) | (1U & (((3U 
                                                   == 
                                                   (3U 
                                                    & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                       >> 2U)))
                                                   ? (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i)
                                                   : 
                                                  ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                   >> 2U)) 
                                                 >> 1U))));
}

void lbc_1___024root___eval(lbc_1___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root___eval\n"); );
    // Body
    lbc_1___024root___combo__TOP__0(vlSelf);
    vlSelf->__Vm_traceActivity[1U] = 1U;
}

#ifdef VL_DEBUG
void lbc_1___024root___eval_debug_assertions(lbc_1___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root___eval_debug_assertions\n"); );
}
#endif  // VL_DEBUG
