// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See lbc_1.h for the primary calling header

#include "verilated.h"

#include "lbc_1___024root.h"

VL_ATTR_COLD void lbc_1___024root___eval_initial(lbc_1___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root___eval_initial\n"); );
}

void lbc_1___024root___combo__TOP__0(lbc_1___024root* vlSelf);

VL_ATTR_COLD void lbc_1___024root___eval_settle(lbc_1___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root___eval_settle\n"); );
    // Body
    lbc_1___024root___combo__TOP__0(vlSelf);
    vlSelf->__Vm_traceActivity[1U] = 1U;
    vlSelf->__Vm_traceActivity[0U] = 1U;
}

VL_ATTR_COLD void lbc_1___024root___final(lbc_1___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root___final\n"); );
}

VL_ATTR_COLD void lbc_1___024root___ctor_var_reset(lbc_1___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root___ctor_var_reset\n"); );
    // Body
    vlSelf->vec_i = VL_RAND_RESET_I(8);
    vlSelf->cnt_o = VL_RAND_RESET_I(3);
    vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i = VL_RAND_RESET_I(4);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        vlSelf->__Vm_traceActivity[__Vi0] = VL_RAND_RESET_I(1);
    }
}
