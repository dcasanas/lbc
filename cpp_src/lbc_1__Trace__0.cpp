// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "lbc_1__Syms.h"


void lbc_1___024root__trace_chg_sub_0(lbc_1___024root* vlSelf, VerilatedVcd::Buffer* bufp);

void lbc_1___024root__trace_chg_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root__trace_chg_top_0\n"); );
    // Init
    lbc_1___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<lbc_1___024root*>(voidSelf);
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    lbc_1___024root__trace_chg_sub_0((&vlSymsp->TOP), bufp);
}

void lbc_1___024root__trace_chg_sub_0(lbc_1___024root* vlSelf, VerilatedVcd::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root__trace_chg_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode + 1);
    // Body
    if (VL_UNLIKELY(vlSelf->__Vm_traceActivity[1U])) {
        bufp->chgCData(oldp+0,((((IData)((3U == (3U 
                                                 & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                    >> 2U)))) 
                                 << 1U) | (1U & (((3U 
                                                   == 
                                                   (3U 
                                                    & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                       >> 2U)))
                                                   ? (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i)
                                                   : 
                                                  ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                   >> 2U)) 
                                                 >> 1U)))),2);
        bufp->chgCData(oldp+1,(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i),4);
        bufp->chgCData(oldp+2,((3U & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                      >> 2U))),2);
        bufp->chgCData(oldp+3,((3U & (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i))),2);
        bufp->chgBit(oldp+4,((1U & (((3U == (3U & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                                   >> 2U)))
                                      ? (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i)
                                      : ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                         >> 2U)) >> 1U))));
        bufp->chgBit(oldp+5,((3U == (3U & ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                           >> 2U)))));
        bufp->chgCData(oldp+6,((3U & ((3U == (3U & 
                                              ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                               >> 2U)))
                                       ? (IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i)
                                       : ((IData)(vlSelf->lbc__DOT____Vcellinp__genblk1__DOT__lbc_inst__vec_i) 
                                          >> 2U)))),2);
    }
    bufp->chgCData(oldp+7,(vlSelf->vec_i),8);
    bufp->chgCData(oldp+8,(vlSelf->cnt_o),3);
    bufp->chgCData(oldp+9,((0xfU & ((IData)(vlSelf->vec_i) 
                                    >> 4U))),4);
    bufp->chgCData(oldp+10,((0xfU & (IData)(vlSelf->vec_i))),4);
    bufp->chgBit(oldp+11,((0xfU == (0xfU & ((IData)(vlSelf->vec_i) 
                                            >> 4U)))));
}

void lbc_1___024root__trace_cleanup(void* voidSelf, VerilatedVcd* /*unused*/) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    lbc_1___024root__trace_cleanup\n"); );
    // Init
    lbc_1___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<lbc_1___024root*>(voidSelf);
    lbc_1__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    vlSymsp->__Vm_activity = false;
    vlSymsp->TOP.__Vm_traceActivity[0U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[1U] = 0U;
}
